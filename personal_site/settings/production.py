from .base import *

DEBUG = False

DBBACKUP_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"

DBBACKUP_STORAGE_OPTIONS = {
    "access_key": env("AWS_ACCESS_KEY_ID"),
    "secret_key": env("AWS_SECRET_ACCESS_KEY"),
    "bucket_name": env("AWS_STORAGE_BUCKET_NAME"),
    "region_name": env("AWS_REGION_NAME", default="eu-central-1"),
    "default_acl": "private",
}

try:
    from .local import *
except ImportError:
    pass
