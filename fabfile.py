import time

from fabric import task

from apps.contrib.fabric.transfers import rsync

app_dir = "/home/apps/mysite"
version = time.strftime("%Y%m%d%H%M%S")

shared_files = [".env"]
shared_dirs = ["public/media"]


def prepare(ctx):
    """Make release folders"""
    print("Check general folders...")
    with ctx.cd(app_dir):
        ctx.run("[ -d releases ] || mkdir releases")
        ctx.run("[ -d shared ] || mkdir shared")
        ctx.run("[ -d logs ] || mkdir logs")

    print("Create release folder...")
    with ctx.cd("{0}/releases".format(app_dir)):
        ctx.run("[ -d {0} ] || mkdir {0}".format(version))
        ctx.run("[ -d {0}/public ] || mkdir {0}/public".format(version))


def sync_code(ctx):
    """Sync code"""
    print("Syncing files...")
    release_dir = "{0}/releases/{1}".format(app_dir, version)
    rsync(
        ctx,
        source="./",
        target=release_dir,
        exclude=(".git/", "assets/", "public/", ".env"),
        rsync_opts='--filter=":- .gitignore"',
    )
    ctx.put(
        "personal_site/static/css/main.css",
        remote=f"{release_dir}/personal_site/static/css/",
    )


def create_venv(ctx):
    """Create virtual environment"""
    print("Create virtual environment...")
    with ctx.cd("{0}/releases/{1}".format(app_dir, version)):
        ctx.run("python3 -m venv .venv")
        ctx.run(".venv/bin/pip install -U setuptools wheel")


def install_deps(ctx):
    """Install project dependencies"""
    print("Installing dependencies...")
    with ctx.cd("{0}/releases/{1}".format(app_dir, version)):
        ctx.run(".venv/bin/pip install -r requirements.txt")


def link_shared(ctx):
    """Link shared files and directories"""
    print("Linking shared data...")
    with ctx.cd("{0}/shared".format(app_dir, version)):
        for filename in shared_files:
            ctx.run(
                "[ -f {0} ] || touch {0} && ln -s {2}/shared/{0} ../releases/{1}/{0}".format(
                    filename, version, app_dir
                )
            )

        for dirname in shared_dirs:
            ctx.run(
                "[ -f {0} ] || mkdir -p {0} && ln -s {2}/shared/{0} ../releases/{1}/{0}".format(
                    dirname, version, app_dir
                )
            )


def run_manage_tasks(ctx):
    """Run django manage.py tasks"""
    print("Running manage.py tasks...")
    with ctx.cd("{0}/releases/{1}".format(app_dir, version)):
        ctx.run(".venv/bin/python manage.py collectstatic --no-input")
        ctx.run(".venv/bin/python manage.py migrate --no-input")


def link_release(ctx):
    """Link current release"""
    print("Linking release...")
    with ctx.cd("{0}".format(app_dir)):
        ctx.run("[ -h previous ] && rm previous || echo -n")
        ctx.run("[ -h current ] && mv current previous || echo -n")
        ctx.run("ln -s releases/{} current".format(version))


def reload_app(ctx):
    """Reload application instances"""
    print("Reloading application...")
    ctx.sudo("systemctl reload supervisor.service")


@task
def deploy(ctx):
    """Deploy the application"""
    print("Deploying app version {}...".format(version))

    prepare(ctx)
    sync_code(ctx)
    create_venv(ctx)
    install_deps(ctx)
    link_shared(ctx)
    run_manage_tasks(ctx)
    link_release(ctx)
    reload_app(ctx)

    # todo: cleanup old releases
