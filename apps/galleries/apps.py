from django.apps import AppConfig


class GalleriesAppConfig(AppConfig):
    name = "apps.galleries"
    verbose_name = "Galleries"
