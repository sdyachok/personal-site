from django import template

from apps.galleries.models import GalleryImage

register = template.Library()


@register.inclusion_tag("galleries/partials/random_gallery_images.html")
def random_gallery_images():
    images = GalleryImage.objects.filter(page__live=True).order_by("?")[:5]

    return {
        "gallery_images": images,
    }
