from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.images import get_image_model_string
from wagtail.models import Page, Orderable
from wagtail.search import index
from wagtail_multi_upload.edit_handlers import MultipleImagesPanel
from wagtailmetadata.models import MetadataPageMixin


class GalleriesPage(Page):
    intro = RichTextField(blank=True)

    max_count = 1

    parent_page_types = ["pages.HomePage"]

    content_panels = Page.content_panels + [FieldPanel("intro")]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        galleries = self.get_children().live().order_by("-first_published_at")

        context["galleries"] = galleries
        return context


class GalleryPage(MetadataPageMixin, Page):
    date = models.DateField("Post date")
    intro = RichTextField(blank=True)

    parent_page_types = ["galleries.GalleriesPage"]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
    ]

    content_panels = Page.content_panels + [
        FieldPanel("date"),
        FieldPanel("intro"),
        MultipleImagesPanel("images", label="Gallery images", image_field_name="image"),
    ]

    @property
    def main_image(self):
        return self.images.first()


class GalleryImage(Orderable):
    page = ParentalKey(GalleryPage, on_delete=models.CASCADE, related_name="images")
    image = models.ForeignKey(
        get_image_model_string(), on_delete=models.CASCADE, related_name="+"
    )
    title = models.CharField(blank=True, max_length=250)

    panels = [
        FieldPanel("image"),
        FieldPanel("title"),
    ]
