from django.apps import AppConfig


class ContribAppConfig(AppConfig):
    name = "apps.contrib"
    verbose_name = "Contrib"
