import datetime as dt
import os
import uuid

from django.db import models
from wagtail.images.models import Image as BaseImage, AbstractImage, AbstractRendition


class AppImage(AbstractImage):
    admin_form_fields = BaseImage.admin_form_fields

    def get_upload_to(self, filename):
        filename = self.file.field.storage.get_valid_name(filename)

        _, ext = os.path.splitext(filename)

        filename = str(uuid.uuid4())
        base_path = "images/original"

        return "{}/{}/{}{}".format(
            base_path, dt.datetime.now().strftime("%Y/%m"), filename, ext
        )


class AppRendition(AbstractRendition):
    image = models.ForeignKey(
        AppImage, on_delete=models.CASCADE, related_name="renditions"
    )

    class Meta:
        unique_together = (("image", "filter_spec", "focal_point_key"),)

    def get_upload_to(self, filename):
        base_path = "images/renditions"

        filename = self.file.field.storage.get_valid_name(filename)

        return "{}/{}/{}".format(
            base_path, dt.datetime.now().strftime("%Y/%m"), filename
        )
