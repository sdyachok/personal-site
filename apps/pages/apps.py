from django.apps import AppConfig


class PagesAppConfig(AppConfig):
    name = "apps.pages"
    verbose_name = "General pages"
