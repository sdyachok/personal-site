from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.fields import RichTextField
from wagtail.images import get_image_model_string
from wagtail.models import Page, Orderable
from wagtail.snippets.models import register_snippet
from wagtailmetadata.models import MetadataPageMixin


@register_snippet
class SocialLink(models.Model):
    icon = models.CharField(
        "Icon class",
        max_length=25,
        help_text="The Fontawesome brand icon class, including prefix. "
        "E.g. 'fa-github-square'",
    )
    title = models.CharField("Link title", max_length=50)
    url = models.URLField("Link", max_length=100)

    panels = [
        FieldPanel("icon"),
        FieldPanel("title"),
        FieldPanel("url"),
    ]

    def __str__(self):
        return self.title


class HomePage(MetadataPageMixin, Page):
    headline = models.CharField("Headline", max_length=300, blank=True)
    intro = RichTextField(blank=True)
    photo = models.ForeignKey(
        get_image_model_string(),
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
        verbose_name="Personal photo",
    )

    max_count = 1

    content_panels = Page.content_panels + [
        FieldPanel("photo"),
        FieldPanel("headline"),
        FieldPanel("intro"),
        InlinePanel("social_links", label="Social Links"),
    ]


class AboutPage(MetadataPageMixin, Page):
    photo = models.ForeignKey(
        get_image_model_string(),
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
        verbose_name="Personal photo",
    )
    headline = models.CharField("Headline", max_length=300, blank=True)
    content = RichTextField(blank=True)

    max_count = 1

    content_panels = Page.content_panels + [
        FieldPanel("photo"),
        FieldPanel("headline"),
        InlinePanel("social_links", label="Social Links"),
        FieldPanel("content"),
    ]


class HomePageSocialLinks(Orderable, models.Model):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name="social_links")
    link = models.ForeignKey(SocialLink, on_delete=models.CASCADE, related_name="+")

    panels = [
        FieldPanel("link"),
    ]

    class Meta(Orderable.Meta):
        verbose_name = "social link"
        verbose_name_plural = "social links"

    def __str__(self):
        return self.link.title


class AboutPageSocialLinks(Orderable, models.Model):
    page = ParentalKey(AboutPage, on_delete=models.CASCADE, related_name="social_links")
    link = models.ForeignKey(SocialLink, on_delete=models.CASCADE, related_name="+")

    panels = [
        FieldPanel("link"),
    ]

    class Meta(Orderable.Meta):
        verbose_name = "social link"
        verbose_name_plural = "social links"

    def __str__(self):
        return self.link.title
