import random

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag(name="random")
def random_string(*args):
    return random.choice(args)


@register.inclusion_tag("pages/partials/cloudflare_web_analytics.html")
def cloudflare_web_analytics():
    return {
        "token": settings.CLOUDFLARE_WEB_ANALYTICS_TOKEN,
    }
