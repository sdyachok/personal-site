from django.apps import AppConfig


class SearchAppConfig(AppConfig):
    name = "apps.search"
    verbose_name = "Search"
