import re

from ipware import get_client_ip

from .models import UserVisit

SKIP_URLS = r"^\/(?:admin|media|static|django)"


class UserVisitMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        path = request.path

        if response.status_code == 200 and re.match(SKIP_URLS, path) is None:
            client_ip, _ = get_client_ip(request)
            user_agent = request.META["HTTP_USER_AGENT"]
            http_referer = request.META.get("HTTP_REFERER")

            UserVisit.objects.create(
                path=path,
                client_ip=client_ip,
                user_agent=user_agent,
                referer=http_referer,
            )

        return response
