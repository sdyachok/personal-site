from django.contrib import admin

from .models import UserVisit


@admin.register(UserVisit)
class UserVisitAdmin(admin.ModelAdmin):
    date_hierarchy = "created_at"

    search_fields = ("path", "client_ip")

    list_display = ("path", "client_ip", "user_agent", "created_at")

    readonly_fields = ("path", "client_ip", "user_agent", "referer", "created_at")
