from django.apps import AppConfig


class StatsAppConfig(AppConfig):
    name = "apps.stats"
    verbose_name = "Statistics"
