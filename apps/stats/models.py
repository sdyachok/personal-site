import uuid

from django.db import models


class UserVisit(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    path = models.CharField("URL path", max_length=600)
    client_ip = models.CharField("Client IP", max_length=64)
    user_agent = models.CharField("UA string", max_length=600)
    referer = models.CharField("HTTP Referer", max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.path

    def __repr__(self):
        return f"<UserVisit path={self.path} client_ip={self.client_ip} created_at={self.created_at}>"
