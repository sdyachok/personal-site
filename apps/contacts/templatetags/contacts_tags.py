from django import template
from django.conf import settings

from apps.contacts.forms import ContactDataForm

register = template.Library()


@register.inclusion_tag("contacts/partials/contact_form.html")
def contact_form():
    form = ContactDataForm()

    return {
        "form": form,
        "cloudflare_turnstile_key": settings.CLOUDFLARE_TURNSTILE_KEY,
    }
