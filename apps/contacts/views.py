from django.conf import settings
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from ipware import get_client_ip

from .forms import ContactDataForm
from .models import ContactData
from .tasks import process_contact_data


@require_POST
def contact_form(request):
    form = ContactDataForm(request.POST)
    message = None

    if form.is_valid():
        contact_data: "ContactData" = form.save(commit=False)

        client_ip, _ = get_client_ip(request)
        contact_data.client_ip = client_ip
        contact_data.user_agent = request.META["HTTP_USER_AGENT"]
        contact_data.referer = request.META["HTTP_REFERER"]

        contact_data.save()

        process_contact_data.delay(contact_data.id)

        message = "Thank you for contacting me! I'll respond as soon as possible."

    if not request.htmx:
        return redirect("/")

    return render(
        request,
        "contacts/partials/contact_form.html",
        {
            "form": form,
            "message": message,
            "cloudflare_turnstile_key": settings.CLOUDFLARE_TURNSTILE_KEY,
        },
    )
