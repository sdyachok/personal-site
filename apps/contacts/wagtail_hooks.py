from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import ContactData


class ContactDataAdmin(ModelAdmin):
    model = ContactData

    list_display = ("name", "email", "subject", "status", "created_at")
    list_filter = ("status",)

    search_fields = ("name", "email", "subject")


modeladmin_register(ContactDataAdmin)
