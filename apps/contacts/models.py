from django.db import models
from wagtail.admin.panels import FieldPanel


class MessageStatus(models.TextChoices):
    NEW = ("new", "New message")
    ON_CHECK = ("on_check", "On check")
    POSSIBLE_SPAM = ("possible_spam", "Possible spam")
    SPAM = ("spam", "Spam")
    CLEAN = ("clean", "Good message")


class ContactData(models.Model):
    status = models.CharField(
        "Status",
        max_length=15,
        choices=MessageStatus.choices,
        default=MessageStatus.NEW,
    )
    name = models.CharField("Name", max_length=50)
    email = models.EmailField("Email", max_length=50)
    subject = models.CharField("Subject", max_length=150)
    message = models.TextField("Message")
    client_ip = models.CharField("Client IP", max_length=32, blank=True, null=True)
    user_agent = models.TextField("Client user agent", blank=True, null=True)
    referer = models.CharField("HTTP Referer", max_length=250, blank=True, null=True)
    created_at = models.DateTimeField("Created at", auto_now_add=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True)

    panels = [
        FieldPanel("status"),
        FieldPanel("name"),
        FieldPanel("email"),
        FieldPanel("subject"),
        FieldPanel("message"),
        FieldPanel("client_ip"),
        FieldPanel("user_agent"),
        FieldPanel("referer"),
    ]

    class Meta:
        verbose_name = "contact data"
        verbose_name_plural = "contact data"

    def __str__(self):
        return self.subject

    @property
    def is_new(self):
        return self.status == MessageStatus.NEW

    @property
    def is_processing(self):
        return self.status == MessageStatus.ON_CHECK

    @property
    def is_spam(self):
        return self.status in [MessageStatus.SPAM, MessageStatus.POSSIBLE_SPAM]

    @property
    def is_ham(self):
        return self.status == MessageStatus.CLEAN
