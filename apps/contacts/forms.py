import logging

import requests
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import ContactData

logger = logging.getLogger(__name__)


def validate_turnstile_response(value):
    verify_url = "https://challenges.cloudflare.com/turnstile/v0/siteverify"

    res = requests.post(
        verify_url,
        data={
            "secret": settings.CLOUDFLARE_TURNSTILE_SECRET,
            "response": value,
        },
    )

    logger.debug("CloudFlare Turnstile response: ", res.raw)

    result = res.json()

    if res.status_code != 200 or not result["success"]:
        raise ValidationError(_("Bad CloudFlare Turnstile response"))


class ContactDataForm(forms.ModelForm):
    turnstile_response = forms.CharField(
        widget=forms.HiddenInput(),
        required=True,
        validators=[validate_turnstile_response],
    )

    class Meta:
        model = ContactData
        fields = ("name", "email", "subject", "message")
