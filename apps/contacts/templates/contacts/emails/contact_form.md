# New Contact

Hi, you have a new contact form submission:

## Contact info

- **Name**: {{ name }}
- **Email**: {{ email }}
- **Subject**: {{ subject }}

## Message

{{ message }}

---
Check admin portal for more details

Yours sincerely,
Serhii Diachok.
