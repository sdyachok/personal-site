from django.apps import AppConfig


class ContactsAppConfig(AppConfig):
    name = "apps.contacts"
    verbose_name = "Contacts"
