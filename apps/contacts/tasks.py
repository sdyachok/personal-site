from akismet import Akismet, SpamStatus
from django.conf import settings
from django_rq import job
from dmdm import send_mail

from .models import ContactData, MessageStatus


def check_spam(contact_data):
    akismet = Akismet(settings.AKISMET_KEY, blog=settings.APP_URL)

    return akismet.check(
        contact_data.client_ip,
        contact_data.user_agent,
        comment_author=contact_data.name,
        comment_author_email=contact_data.email,
        comment_content=contact_data.message,
        comment_type="contact-form",
        referrer=contact_data.referer,
    )


def send_email(contact_data):
    send_mail(
        contact_data.subject,
        "contacts/emails/contact_form.md",
        settings.DEFAULT_FROM_EMAIL,
        [settings.DEFAULT_TO_EMAIL],
        context={
            "name": contact_data.name,
            "email": contact_data.email,
            "subject": contact_data.subject,
            "message": contact_data.message,
        },
    )


@job
def process_contact_data(contact_data_id):
    contact_data = ContactData.objects.get(pk=contact_data_id)

    if not contact_data.is_new:
        return None

    contact_data.status = MessageStatus.ON_CHECK
    contact_data.save(update_fields=["status", "updated_at"])

    result = check_spam(contact_data)

    if result == SpamStatus.Ham:
        contact_data.status = MessageStatus.CLEAN
    elif result == SpamStatus.DefiniteSpam:
        contact_data.status = MessageStatus.SPAM
    else:
        contact_data.status = MessageStatus.POSSIBLE_SPAM

    contact_data.save(update_fields=["status", "updated_at"])

    if contact_data.is_ham:
        send_email(contact_data)

    return contact_data.status
