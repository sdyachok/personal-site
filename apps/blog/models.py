from django.db import models
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from wagtail.admin.panels import FieldPanel, MultiFieldPanel
from wagtail.blocks import RichTextBlock
from wagtail.fields import RichTextField, StreamField
from wagtail.images import get_image_model_string
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Page
from wagtail.search import index
from wagtailcodeblock.blocks import CodeBlock
from wagtailmetadata.models import MetadataPageMixin


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    max_count = 1

    parent_page_types = ["pages.HomePage"]

    content_panels = Page.content_panels + [FieldPanel("intro")]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        articles = self.get_children().live().order_by("-first_published_at")

        tag = request.GET.get("tag")
        if tag:
            articles = articles.filter(blogpage__tags__name=tag)

        context["articles"] = articles
        return context


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        "BlogPage", related_name="tagged_items", on_delete=models.CASCADE
    )


class BlogPage(MetadataPageMixin, Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    image = models.ForeignKey(
        get_image_model_string(),
        on_delete=models.SET_NULL,
        related_name="+",
        null=True,
        blank=True,
    )

    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    body = StreamField(
        [
            ("paragraph", RichTextBlock()),
            ("image", ImageChooserBlock()),
            ("code", CodeBlock(label="Code sample", default_language="python")),
        ],
        use_json_field=True,
    )

    parent_page_types = ["blog.BlogIndexPage"]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
        index.SearchField("body"),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("date"),
                FieldPanel("image"),
                FieldPanel("tags"),
            ],
            heading="Post information",
        ),
        FieldPanel("intro"),
        FieldPanel("body"),
    ]
