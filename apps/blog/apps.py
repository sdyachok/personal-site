from django.apps import AppConfig


class BlogAppConfig(AppConfig):
    name = "apps.blog"
    verbose_name = "Blog"
