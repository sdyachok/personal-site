from django import template

from apps.blog.models import BlogPage


register = template.Library()


@register.inclusion_tag("blog/partials/latest_blog_posts.html")
def latest_blog_posts():
    articles = BlogPage.objects.live().order_by("-first_published_at")[:3]

    return {
        "articles": articles,
    }
