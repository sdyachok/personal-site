# Project ideas and backlog

### Pages
- [x] About me page
  - [ ] Resume attachment
- [x] Blog/articles page
  - [x] Article detail page
- [x] Galleries page
  - [x] Gallery detail page
- [ ] Projects page

### Design
- [x] Home page design
- [x] About me page design
- [x] Blog index design
  - [x] Article detail design
- [x] Gallery index design
  - [x] Gallery detail page design
- [ ] Projects page design
